var express = require('express');
var cors = require('cors');

var app = express();
var mongojs = require('mongojs');
var db = mongojs('acciontraining', ['user']);
var bodyParser = require('body-parser');


app.use(cors());
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

/* Get Employees */
/*
Calling Method : localhost:3000/getEmployees
Body Tag : Nil
Type: Get
*/
app.get('/getEmployees', function(req, res) {
    console.log("Finding Employees -----> ", req.body);
    db.user.find({}, function(err, doc) {
        if (doc && doc.length) {
            res.json(doc);
        } else {
            res.json({
                "status": "Error getting Employee List"
            });
        }
    });
});

/* Add Employee */
/*
Calling Method : localhost:3000/addEmployee
Body Tag : {"Name":"Shishir", "emp_id":1161, "designation": "Senior Software Engineer"}
Type : POST
*/
app.post('/addEmployee', function(req, res) {
    console.log("Adding Employees to db-----> ", req.body);
    db.user.insert(req.body, function(errInsert, insertDoc) {
        if (insertDoc) {
            res.json(insertDoc);
        } else {
            res.json({
                "status": "Error Inserting Employee"
            });
        }
    });
});

/* Update Employee data */
/*
Calling Method : localhost:3000/updateEmployee
Body Tag : [{"empId":"your mongoobject id"},{"Name":"Shishir Shrivastava", "emp_id":1161, "designation": "Senior Software Engineer"}]
Type : POST
*/
app.post('/updateEmployee', function(req, res) {
	console.log("Updating Employees in db----->", req.body);
    reqData = req.body[0];
    empID = reqData.empId;
    if (Object.keys(req.body[1]).length) {
        db.user.findAndModify({
            query: {
                "_id": mongojs.ObjectId(empID)
            },
            update: {
                $set: req.body[1]
            },
            new: true
        }, function(err, doc) {
            console.log(doc);
            if (doc) {
                res.json(doc);
            } else {
                res.json({
                    "status": "Error Updating"
                });
            }
        });
    } else {
        res.json({
            "Status": "Success",
            "Message": "No data found to update"
        });
    }
});

/* Delete Employee data */
/*
Calling Method : localhost:3000/deleteEmployee
Body Tag : [{"empId":"your mongoobject id"}]
Type : POST
*/
app.post('/deleteEmployee', function(req, res) {
	console.log("Deleting Employees in db----->", req.body);
    empID = req.body[0].empId;
    db.user.remove({
        "_id": new mongojs.ObjectId(empID)
    }, function(err, doc) {
        if (doc) {
            if (doc.n == 0) {
                res.json({
                    "status": "Success",
                    "message": "Requested employee id doesn't exist."
                });
            } else {
                res.json({
                    "status": "Success",
                    "message": "Record deleted successfully."
                });
            }
        } else {
            res.json({
                "status": "Error",
                "message": "Could not delete. Please try later."
            });
        }
    });
});

app.listen(3000, '0.0.0.0');
console.log('App running at 3000');